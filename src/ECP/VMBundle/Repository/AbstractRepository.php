<?php
/**
 * Created by PhpStorm.
 * User: aj
 * Date: 26.12.2017
 * Time: 2:47
 */

namespace ECP\VMBundle\Repository;


use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityRepository;

abstract class AbstractRepository extends EntityRepository
{
    const ORDER_ASC = 1;
    const ORDER_DESC = 2;

    const ORDER_MAP = [
        self::ORDER_ASC => 'ASC',
        self::ORDER_DESC => 'DESC'
    ];

    /**
     * @return \Doctrine\DBAL\Connection
     */
    protected function getDBALConnection(): Connection
    {
        return $this->getEntityManager()->getConnection();
    }
}