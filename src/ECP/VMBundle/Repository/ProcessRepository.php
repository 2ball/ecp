<?php
/**
 * Created by PhpStorm.
 * User: aj
 * Date: 03.01.2018
 * Time: 12:32
 */

namespace ECP\VMBundle\Repository;

class ProcessRepository extends AbstractRepository
{
    /**
     * @return int
     */
    public function getCurrentBalance()
    {
        $balance = $this->getDBALConnection()->fetchColumn("SELECT COALESCE(SUM(amount), 0) AS `sum` FROM process");

        return !$balance ? 0 : $balance;
    }

    public function reset()
    {
        $this->getDBALConnection()->executeQuery("DELETE FROM process");
    }
}