<?php
/**
 * Created by PhpStorm.
 * User: aj
 * Date: 03.01.2018
 * Time: 12:31
 */

namespace ECP\VMBundle\Repository;

class VMCoinStorageRepository extends AbstractRepository
{
    /**
     * @param int $denomination
     * @return bool
     */
    public function addCoinByDenomination(int $denomination): bool
    {
        $dbal = $this->getDBALConnection();

        try {
            $dbal->beginTransaction();
            $dbal->executeQuery("UPDATE purse SET quantity = quantity - 1 WHERE coin_denomination = ?;", [$denomination]);
            $dbal->executeQuery("INSERT INTO vm_coin_storage VALUES(?, 1) ON DUPLICATE KEY UPDATE quantity = quantity + 1", [$denomination]);
            $dbal->executeQuery("INSERT INTO process (amount, created_at) VALUES(?, NOW())", [$denomination]);
            $dbal->commit();

            return true;
        } catch(\Exception $e) {
            $dbal->rollBack();

            return false;
        }
    }

    /**
     * @param array $coinsForReturnMap
     * @return bool
     */
    public function returnCoinsByMap(array $coinsForReturnMap)
    {
        $dbal = $this->getDBALConnection();

        try {
            $dbal->beginTransaction();

            foreach ($coinsForReturnMap as $denomination => $quantity) {
                $dbal->executeQuery("UPDATE vm_coin_storage SET quantity = quantity - ? WHERE coin_denomination = ?;", [$quantity, $denomination]);

                $amount = $denomination * -1;
                for ($i = 0; $i < $quantity; $i++) {
                    $dbal->executeQuery("INSERT INTO process (amount, created_at) VALUES(?, NOW());", [$amount]);
                }

                $dbal->executeQuery("INSERT INTO purse VALUES(?, ?) ON DUPLICATE KEY UPDATE quantity = quantity + VALUES(quantity);", [$denomination, $quantity]);
            }

            $dbal->commit();

            return true;
        } catch(\Exception $e) {
            $dbal->rollBack();

            return false;
        }
    }

    /**
     * @param $order int
     * @return array
     */
    public function getAvailableCoinsMap($order = self::ORDER_ASC): array
    {
        $dbal = $this->getDBALConnection();
        $dbal->setFetchMode(\PDO::FETCH_KEY_PAIR);

        return $dbal->fetchAll(sprintf("SELECT coin_denomination, quantity FROM vm_coin_storage WHERE quantity > 0 ORDER BY coin_denomination %s", self::ORDER_MAP[$order]));
    }

    public function reset()
    {
        $dbal = $this->getDBALConnection();

        $dbal->executeQuery("UPDATE vm_coin_storage SET quantity = ? WHERE coin_denomination = ?", [100, 1]);
        $dbal->executeQuery("UPDATE vm_coin_storage SET quantity = ? WHERE coin_denomination = ?", [100, 2]);
        $dbal->executeQuery("UPDATE vm_coin_storage SET quantity = ? WHERE coin_denomination = ?", [100, 5]);
        $dbal->executeQuery("UPDATE vm_coin_storage SET quantity = ? WHERE coin_denomination = ?", [100, 10]);
    }
}