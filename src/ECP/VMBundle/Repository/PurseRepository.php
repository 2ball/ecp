<?php
/**
 * Created by PhpStorm.
 * User: aj
 * Date: 26.12.2017
 * Time: 2:44
 */

namespace ECP\VMBundle\Repository;

use ECP\VMBundle\Entity\Purse;

class PurseRepository extends AbstractRepository
{
    /**
     * @return Purse[]
     */
    public function getPurseCoinsList()
    {
        return $this
                ->createQueryBuilder('purse')
                ->select('purse, coin')
                ->innerJoin('purse.coin', 'coin')
                ->where('purse.quantity > 0')
                ->orderBy('coin.denomination', 'ASC')
                ->getQuery()
                ->getResult();
    }

    /**
     * @return array
     */
    public function getAvailableCoinsMap()
    {
        $dbal = $this->getDBALConnection();
        $dbal->setFetchMode(\PDO::FETCH_KEY_PAIR);

        return $dbal->fetchAll("SELECT coin_denomination, quantity FROM purse");
    }

    public function reset()
    {
        $dbal = $this->getDBALConnection();

        $dbal->executeQuery("UPDATE purse SET quantity = ? WHERE coin_denomination = ?", [10, 1]);
        $dbal->executeQuery("UPDATE purse SET quantity = ? WHERE coin_denomination = ?", [30, 2]);
        $dbal->executeQuery("UPDATE purse SET quantity = ? WHERE coin_denomination = ?", [20, 5]);
        $dbal->executeQuery("UPDATE purse SET quantity = ? WHERE coin_denomination = ?", [15, 10]);
    }
}