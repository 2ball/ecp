<?php
/**
 * Created by PhpStorm.
 * User: aj
 * Date: 26.12.2017
 * Time: 2:44
 */

namespace ECP\VMBundle\Repository;

use ECP\VMBundle\Entity\Product;

class ProductRepository extends AbstractRepository
{
    /**
     * @return Product[]
     */
    public function getProductsList(): array
    {
        return $this
                ->createQueryBuilder('product')
                ->orderBy('product.price', 'ASC')
                ->getQuery()
                ->getResult();
    }

    public function buyProductById(int $productId): bool
    {
        $dbal = $this->getDBALConnection();

        try {
            $dbal->beginTransaction();

            $dbal->executeQuery("UPDATE product SET stock = stock - 1 WHERE id = ?", [$productId]);
            $dbal->executeQuery("INSERT INTO process (amount, is_buy_product, created_at) VALUES((SELECT price * -1 FROM product WHERE id = ?), 1, NOW())", [$productId]);

            $dbal->commit();

            return true;
        } catch(\Exception $e) {
            $dbal->rollBack();

            return false;
        }
    }

    public function reset()
    {
        $dbal = $this->getDBALConnection();

        $dbal->executeQuery("UPDATE product SET stock = ? WHERE id = ?", [10, 1]);
        $dbal->executeQuery("UPDATE product SET stock = ? WHERE id = ?", [20, 2]);
        $dbal->executeQuery("UPDATE product SET stock = ? WHERE id = ?", [20, 3]);
        $dbal->executeQuery("UPDATE product SET stock = ? WHERE id = ?", [15, 4]);
    }
}