<?php
/**
 * Created by PhpStorm.
 * User: aj
 * Date: 26.12.2017
 * Time: 0:39
 */

namespace ECP\VMBundle\Entity;

use Doctrine\ORM\Mapping AS ORM;

/**
 * @ORM\Table(name="coin")
 * @ORM\Entity()
 */
class Coin
{
    /**
     * @var integer
     *
     * @ORM\Column(name="denomination", type="integer", nullable=false)
     * @ORM\Id()
     */
    private $denomination;


    /**
     * @return int
     */
    public function getDenomination(): int
    {
        return $this->denomination;
    }

    /**
     * @param int $denomination
     */
    public function setDenomination(int $denomination)
    {
        $this->denomination = $denomination;
    }
}