<?php
/**
 * Created by PhpStorm.
 * User: aj
 * Date: 26.12.2017
 * Time: 1:01
 */

namespace ECP\VMBundle\Entity;

use Doctrine\ORM\Mapping AS ORM;

/**
 * @ORM\Table(name="purse")
 * @ORM\Entity(repositoryClass="ECP\VMBundle\Repository\PurseRepository")
 */
class Purse
{
    /**
     * @var Coin
     *
     * @ORM\ManyToOne(targetEntity="Coin")
     * @ORM\JoinColumn(name="coin_denomination", referencedColumnName="denomination")
     * @ORM\Id()
     */
    private $coin;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantity", type="integer", nullable=false, options={"unsigned": true, "default": 0})
     */
    private $quantity = 0;


    /**
     * @return Coin
     */
    public function getCoin(): Coin
    {
        return $this->coin;
    }

    /**
     * @param Coin $coin
     */
    public function setCoin(Coin $coin)
    {
        $this->coin = $coin;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity(int $quantity)
    {
        $this->quantity = $quantity;
    }
}