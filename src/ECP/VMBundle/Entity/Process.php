<?php
/**
 * Created by PhpStorm.
 * User: aj
 * Date: 03.01.2018
 * Time: 12:30
 */

namespace ECP\VMBundle\Entity;

use Doctrine\ORM\Mapping AS ORM;

/**
 * @ORM\Table(name="process")
 * @ORM\Entity(repositoryClass="ECP\VMBundle\Repository\ProcessRepository")
 */
class Process
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Id()
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="amount", type="integer", nullable=false, options={"default": 0})
     */
    private $amount;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_buy_product", type="boolean", nullable=false, options={"default": 0})
     */
    private $buyProduct = false;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;


    /**
     * Balance constructor.
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     * @param int $amount
     */
    public function setAmount(int $amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return bool
     */
    public function isBuyProduct(): bool
    {
        return $this->buyProduct;
    }

    /**
     * @param bool $buyProduct
     */
    public function setBuyProduct(bool $buyProduct)
    {
        $this->buyProduct = $buyProduct;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
    }
}