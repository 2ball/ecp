<?php

namespace ECP\VMBundle\Controller;

use ECP\VMBundle\Repository\AbstractRepository;
use ECP\VMBundle\Repository\ProcessRepository;
use ECP\VMBundle\Repository\ProductRepository;
use ECP\VMBundle\Repository\PurseRepository;
use ECP\VMBundle\Repository\VMCoinStorageRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DefaultController extends Controller
{
    /**
     * @Route("/")
     * @Template()
     *
     * @return array
     */
    public function indexAction()
    {
        $purse = $this->getPurseRepository()->getPurseCoinsList();
        $products = $this->getProductRepository()->getProductsList();
        $balance = $this->getCurrentBalance();

        return compact('purse', 'products', 'balance');
    }

    /**
     * @Route("/add-coin/{denomination}")
     *
     * @param $request Request
     * @param $denomination integer
     * @return RedirectResponse | JsonResponse
     */
    public function addCoinAction(Request $request, int $denomination)
    {
        try {
            $coinsMap = $this->getPurseRepository()->getAvailableCoinsMap();
            if (!isset($coinsMap[$denomination])) {
                //Возвращаем монету в виде сдачи
                throw new \Exception(sprintf('Монеты достоинством %d руб. не принимаются', $denomination));
            }

            if (!($coinsMap[$denomination] > 0)) {
                throw new \Exception(sprintf('В вашем кошельке нет монет достоинством %d руб.', $denomination));
            }

            $addCoinSuccess = $this->getVMCoinStorageRepository()->addCoinByDenomination($denomination);
            if (!$addCoinSuccess) {
                //Возвращаем монету в виде сдачи
                throw new \Exception(sprintf('Во время внесения произошла ошибка'));
            }

            $balance = $this->getProcessRepository()->getCurrentBalance();

            if ($request->isXmlHttpRequest()) {
                return new JsonResponse(['success' => true, 'balance' => $balance]);
            }

            return $this->getRedirectToDefaultResponse();

        } catch(\Exception $e) {
            if ($request->isXmlHttpRequest()) {
                return new JsonResponse(['success' => false, 'modal' => $this->getRenderedModal($e->getMessage())]);
            }

            $this->get('session')->getFlashBag()->add('error', $e->getMessage());

            return $this->getRedirectToDefaultResponse();
        }
    }

    /**
     * @Route("/buy-product/{productId}")
     *
     * @param $request Request
     * @param $productId integer
     * @return RedirectResponse | JsonResponse
     */
    public function buyProductAction(Request $request, int $productId)
    {
        try {
            $productRepository = $this->getProductRepository();
            $product = $productRepository->find($productId);
            if (!$product) {
                throw new NotFoundHttpException();
            }

            if (!($product->getStock() > 0)) {
                throw new \Exception(sprintf("Товара \"%s\" нет в наличии", $product->getName()));
            }

            $balance = $this->getCurrentBalance();
            if ($product->getPrice() > $balance) {
                throw new \Exception(sprintf("Недостаточно средств для покупки %s. Внесите еще %d руб. или заберите сдачу", $product->getName(), $product->getPrice() - $balance));
            }

            $success = $productRepository->buyProductById($product->getId());
            if (!$success) {
                throw new \Exception("Произошла непредвиденная ошибка");
            }

            $balance = $this->getCurrentBalance();
            $message = sprintf("Спасибо за покупку! Возьмите Ваш %s", $product->getName());

            if ($request->isXmlHttpRequest()) {
                return new JsonResponse(['success' => true, 'modal' => $this->getRenderedModal($message), 'balance' => $balance]);
            }

            $this->get('session')->getFlashBag()->add('success', $message);

            return $this->getRedirectToDefaultResponse();
        } catch (\Exception $e) {
            if ($request->isXmlHttpRequest()) {
                return new JsonResponse(['success' => false, 'modal' => $this->getRenderedModal($e->getMessage())]);
            }

            $this->get('session')->getFlashBag()->add('error', $e->getMessage());

            return $this->getRedirectToDefaultResponse();
        }
    }

    /**
     * @Route("/return-coins")
     *
     * @param $request Request
     * @return RedirectResponse | JsonResponse
     */
    public function returnCoinsAction(Request $request)
    {
        try {
            $coinsForReturnAndSurrender = $this->getCoinsForReturnSurrenderAndDiff();

            if ($coinsForReturnAndSurrender[1] == 0 || $request->query->getBoolean('dirty-return')) {
                $success = $this->getVMCoinStorageRepository()->returnCoinsByMap($coinsForReturnAndSurrender[0]);
                if (!$success) {
                    throw new \Exception('Произошла непредвиденная ошибка');
                }
            } else {
                throw new \Exception(sprintf('Невозможно вернуть сдачу в полном объеме. Вернуть %d оставив %d на балансе?', $coinsForReturnAndSurrender[2], $coinsForReturnAndSurrender[1]));
            }

            if ($request->isXmlHttpRequest()) {
                return new JsonResponse(['success' => $success, 'balance' => $coinsForReturnAndSurrender[1], 'returnedCoins' => $coinsForReturnAndSurrender[0]]);
            }

            return $this->getRedirectToDefaultResponse();
        } catch (\Exception $e) {
            if ($request->isXmlHttpRequest()) {
                return new JsonResponse(['success' => false, 'modal' => $this->getRenderedModal($e->getMessage())]);
            }

            $this->get('session')->getFlashBag()->add('error', $e->getMessage());

            return $this->getRedirectToDefaultResponse();
        }
    }

    /**
     * @Route("/vm-coins")
     *
     * @param $request Request
     * @return Response | JsonResponse
     */
    public function vmCoinsAction(Request $request)
    {
        $vmAvailableCoins = $this->getVMCoinStorageRepository()->getAvailableCoinsMap();

        $responseContent = $this->renderView('@ECPVM/default/vmcoins.html.twig', ['vmAvailableCoins' => $vmAvailableCoins]);
        if ($request->isXmlHttpRequest()) {
            return new JsonResponse(['success' => true, 'content' => $responseContent]);
        }

        return new Response($responseContent);
    }

    /**
     * @Route("/reset")
     *
     * @return RedirectResponse
     */
    public function resetAction()
    {
        $this->getDoctrine()->getRepository('ECPVMBundle:Process')->reset();
        $this->getDoctrine()->getRepository('ECPVMBundle:Product')->reset();
        $this->getDoctrine()->getRepository('ECPVMBundle:Purse')->reset();
        $this->getDoctrine()->getRepository('ECPVMBundle:VMCoinStorage')->reset();

        return $this->getRedirectToDefaultResponse();
    }


    /**
     * @return PurseRepository
     */
    protected function getPurseRepository(): PurseRepository
    {
        return $this->getDoctrine()->getRepository('ECPVMBundle:Purse');
    }

    /**
     * @return ProcessRepository
     */
    protected function getProcessRepository(): ProcessRepository
    {
        return $this->getDoctrine()->getRepository('ECPVMBundle:Process');
    }

    /**
     * @return ProductRepository
     */
    protected function getProductRepository(): ProductRepository
    {
        return $this->getDoctrine()->getRepository('ECPVMBundle:Product');
    }

    /**
     * @return VMCoinStorageRepository
     */
    protected function getVMCoinStorageRepository(): VMCoinStorageRepository
    {
        return $this->getDoctrine()->getRepository('ECPVMBundle:VMCoinStorage');
    }


    /**
     * @return int
     */
    protected function getCurrentBalance(): int
    {
        return $this->getProcessRepository()->getCurrentBalance();
    }

    /**
     * @return RedirectResponse
     */
    protected function getRedirectToDefaultResponse(): RedirectResponse
    {
        return new RedirectResponse($this->generateUrl('ecp_vm_default_index'));
    }

    /**
     * @return array
     */
    protected function getCoinsForReturnSurrenderAndDiff(): array
    {
        $availableCoinsMap = $this->getDoctrine()->getRepository('ECPVMBundle:VMCoinStorage')->getAvailableCoinsMap(AbstractRepository::ORDER_DESC);
        $balance = $surrender = $this->getDoctrine()->getRepository('ECPVMBundle:Process')->getCurrentBalance();

        $coinsForReturn = [];
        foreach ($availableCoinsMap as $denomination => $quantity) {
            if ($denomination > $surrender) {
                continue;
            }

            $coinsForReturn[$denomination] = min(floor($surrender / $denomination), $quantity);
            $surrender -= $denomination * $coinsForReturn[$denomination];
        }

        return [$coinsForReturn, $surrender, $balance - $surrender];
    }

    /**
     * @param string $text
     * @return string
     */
    protected function getRenderedModal(string $text): string
    {
        return $this->renderView("__modal.html.twig", ['text' => $text]);
    }
}
